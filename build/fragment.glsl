#version 330

#define float2 vec2
#define float3 vec3
#define float4 vec4
#define float4x4 mat4
#define float3x3 mat3

in float2 fragmentTexCoord;

layout(location = 0) out vec4 fragColor;

uniform int g_screenWidth;
uniform int g_screenHeight;

uniform float3 g_bBoxMin   = float3(-1,-1,-1);
uniform float3 g_bBoxMax   = float3(+1,+1,+1);

uniform float4x4 g_rayMatrix;

uniform float4   g_bgColor = float4(0,0,1,1);

float3 EyeRayDir(float x, float y, float w, float h)
{
	float fov = 3.141592654f/(2.0f); 
  float3 ray_dir;
  
	ray_dir.x = x+0.5f - (w/2.0f);
	ray_dir.y = y+0.5f - (h/2.0f);
	ray_dir.z = -(w)/tan(fov/2.0f);
	
  return normalize(ray_dir);
}

float fmin(float a, float b, int x, inout int c) {
	if (a <= b)
		return a;
	else {
		c = x;
		return b;
	}
}

float distance_to_scene(float3 intersection_point, int x) {
	if (x == 0) {
		float3 p = intersection_point - float3(-5, 0, 0);
		float s = 2;
		return length(p) - s;
	}
	else if (x == 1) {
		float3 p = intersection_point - float3(0, 0, 0);
		float3 b = float3(2, 2, 1);
		vec3 d = abs(p) - b;
  	return length(max(d,0.0))
         + min(max(d.x,max(d.y,d.z)),0.0);
	}
	else if (x == 2) {
		float3 p = intersection_point - float3(10, 0, 0);
		float ra = 2;
		float rb = 1;
		float h = 2; 
		vec2 d = vec2( length(p.xz)-2.0*ra+rb, abs(p.y) - h );
    return min(max(d.x,d.y),0.0) + length(max(d,0.0)) - rb;
	}
	else if (x == 3) {
		float3 p = intersection_point - float3(0, -7, 0);
		float3 r = float3(2, 3, 3);
		float k0 = length(p/r);
    float k1 = length(p/(r*r));
    return k0*(k0-1.0)/k1;
	}
	else if (x == 4) {
		float3 p = intersection_point - float3(0, 5, 0);
		float s = 2;
		p = abs(p);
    float m = p.x+p.y+p.z-s;
    vec3 q;
    if( 3.0*p.x < m ) q = p.xyz;
    else if( 3.0*p.y < m ) q = p.yzx;
    else if( 3.0*p.z < m ) q = p.zxy;
    else return m*0.57735027;
    
    float k = clamp(0.5*(q.z-q.y+s),0.0,s); 
    return length(vec3(q.x,q.y-s+k,q.z-k)); 
	}
}

float3 FindNormal(float3 z, int x) {
	float eps = 0.0001;
	float3 z1 = z + float3(eps, 0, 0);
	float3 z2 = z - float3(eps, 0, 0);
	float3 z3 = z + float3(0, eps, 0);
	float3 z4 = z - float3(0, eps, 0);
	float3 z5 = z + float3(0, 0, eps);
	float3 z6 = z - float3(0, 0, eps);
	float dx = distance_to_scene(z1, x) - distance_to_scene(z2, x);
	float dy = distance_to_scene(z3, x) - distance_to_scene(z4, x);
	float dz = distance_to_scene(z5, x) - distance_to_scene(z6, x);
	return normalize(float3(dx, dy, dz) / (2.0*eps));
}

float4 Phong_shading_model(float3 pos, float3 dir, float3 intersection_point, int intersection_int, float4 light_source[2]) {

	// http://steps3d.narod.ru/tutorials/lighting-tutorial.html

	float3 l[2] = float3[2](normalize ( vec3 ( light_source[0].xyz ) - intersection_point), normalize ( vec3 ( light_source[1].xyz ) - intersection_point));                    // vector to light source
  float3 v = normalize ( vec3 ( pos )   - intersection_point );                    // vector to the eye
  float3 n = FindNormal (intersection_point, intersection_int); 

	vec4  diffColor;
  vec4  specColor; 
  float specPower;

  if (intersection_int == 0) {
		diffColor = vec4 ( 0.5, 0.0, 0.0, 1.0 );
		specColor = vec4 ( 0.7, 0.7, 0.0, 1.0 );
  	specPower = 30.0;
	}
	else if (intersection_int == 1) {
		diffColor = vec4 ( 0.0, 0.5, 0.0, 1.0 );
		specColor = vec4 ( 0.5, 0.5, 0.5, 1.0 );
  	specPower = 10.0;
	}
	else if (intersection_int == 2) {
		diffColor = vec4 ( 0.5, 0.0, 0.1, 1.0 );
		specColor = vec4 ( 0.3, 0.1, 0.9, 1.0 );
  	specPower = 30.0;
	}
	else if (intersection_int == 3) {
		diffColor = vec4 ( 0.6, 0.5, 0.4, 1.0 );
		specColor = vec4 ( 0.7, 0.7, 0.0, 1.0 );
  	specPower = 30.0;
	}
	else if (intersection_int == 4) {
		diffColor = vec4 ( 0.5, 0.4, 0.0, 1.0 );
		specColor = vec4 ( 0.7, 0.7, 0.0, 1.0 );
  	specPower = 30.0;
	}

  vec3 n2   = normalize ( n );
  vec3 l2[2]  = vec3[2](normalize ( l[0] ), normalize(l[1]));
  vec3 v2   = normalize ( v );
  vec3 r    = reflect ( -v2, n2 );
  vec4 diff[2] = vec4[2](diffColor * max ( dot ( n2, l2[0] ), 0.0 ), diffColor * max ( dot ( n2, l2[1] ), 0.0 ));
  vec4 spec[2] = vec4[2](specColor * pow ( max ( dot ( l2[0], r ), 0.0 ), specPower ), specColor * pow ( max ( dot ( l2[1], r ), 0.0 ), specPower ));

	return diff[0] + diff[1] + spec[0] + spec[1];
}

float distance_aided_ray_matching(float3 intersection_point, inout int intersection_int, float tmax) {
	float ditance_to_nearest = tmax;
	float distance_to_x;
	intersection_int = 0;
	for (int x = 0; x < 5; x++)
		ditance_to_nearest = fmin(ditance_to_nearest, distance_to_scene(intersection_point, x), x, intersection_int);
	return ditance_to_nearest;
}

bool Intersect(float3 pos, float3 dir, inout float3 intersection_point, inout int intersection_int, float tmin, float tmax) {

	intersection_point = pos;

	float total_distance = 0, iteration_distance;

	while (total_distance < tmax) {

		iteration_distance = distance_aided_ray_matching(intersection_point, intersection_int, tmax);

		total_distance += iteration_distance;

		intersection_point += dir * iteration_distance;

		if (iteration_distance <= tmin)
			return true;
	}

	return false;
}

float4 Ray_Trace(float3 pos, float3 dir, float tmin, float tmax, float4 light_source[2]) {

	float3 intersection_point = pos;
	int intersection_int = 0;

	if (!Intersect(pos, dir, intersection_point, intersection_int, tmin, tmax))
		return g_bgColor;

	return Phong_shading_model(pos, dir, intersection_point, intersection_int, light_source);
}

void main(void)
{	

  float w = float(g_screenWidth);
  float h = float(g_screenHeight);
  
  // get curr pixelcoordinates
  //
  float x = fragmentTexCoord.x*w; 
  float y = fragmentTexCoord.y*h;
  
  // generate initial ray
  //
  float3 ray_pos = float3(0,0,0); 
  float3 ray_dir = EyeRayDir(x,y,w,h);
 
  // transorm ray with matrix
  //
  ray_pos = (g_rayMatrix*float4(ray_pos,1)).xyz;
  ray_dir = float3x3(g_rayMatrix)*ray_dir;
 
  // intersect bounding box of the whole scene, if no intersection found return background color
  // 
  float tmin = 0.1;
  float tmax = 100;
  
  float4 light_source[2];

  light_source[0] = float4(100, 100, 100, 0);
	light_source[1] = float4(-100, 100, 100, 0);


  fragColor = 1.2 * Ray_Trace(ray_pos, ray_dir, tmin, tmax, light_source);
}


